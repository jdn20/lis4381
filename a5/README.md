> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Assignment 5 Requirements:

*4 Parts:*

1. Local web app server-side validation
2. Skillset 13 - Sphere Volume
3. Skillset 14 - Simple Calculator
4. Skillset 15 - Write/Read File

#### README.md file should include the following items:

* Screenshots of local web app with valid and invalid data
* Screenshots of skillsets
* Link to local lis4381: http://localhost:8080/repos/lis4381/
  


> 
> 
> 
>


#### Assignment Screenshots:

*Screenshot of A5 Passed Validation:*

<table>
    <tr>
        <th>Process</th>
        <td><img src="img/a5_1.png"></td>
    </tr>
    <tr>
        <th>Index</th>
        <td><img src="img/a5_2.png"></td>
    </tr>
</table>

&nbsp;

*Screenshot of A5 Failed Validation:*

<table>
    <tr>
        <th>Process</th>
        <td><img src="img/a5_3.png"></td>
    </tr>
    <tr>
        <th>Error</th>
        <td><img src="img/a5_4.png"></td>
    </tr>
</table>

&nbsp; 

*Screenshot of Skillset 13 - Sphere Volume:*
![Skillset 13](img/ss13.png)

&nbsp;

*Screenshot of Skillset 14 - Simple Calculator:*

<table>
    <tr>
        <th>Index</th>
        <td><img src="img/ss14_1.png"></td>
    </tr>
    <tr>
        <th>Process Function</th>
        <td><img src="img/ss14_2.png"></td>
    </tr>
    <tr>
        <th>Failed Validation</th>
        <td><img src="img/ss14_4.png"></td>
    </tr>
</table>

&nbsp;

*Screenshot of Skillset 15 - Write/Read File:*

<table>
    <tr>
        <th>Index</th>
        <td><img src="img/ss15_index.png"></td>
    </tr>
    <tr>
        <th>Process</th>
        <td><img src="img/ss15_process.png"></td>
    </tr>
</table>

