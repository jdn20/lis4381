> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Assignment 2 Requirements:

*Two parts:*

1. Healthy Recipes App on Android Studio
2. Skillsets 1-3

#### README.md file should include the following items:

* Course title, your name, assignment requirements
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.

#### Assignment Screenshots:

*Screenshot of Healthy Recipes App*:

<table> 
    <tr>
        <td><img src="img/recipe.png"></td>
        <td><img src="img/recipe2.png"></td>
    </tr>
</table> 


*Screenshot of Skillset 1*:

![Skillset 1 - Even and odd numbers](img/evenodd.png)

*Screenshot of Skillset 2*:

![Skillset 2 - Largest number](img/largestnumber.png)

*Screenshot of Skillset 3*:

![Skillset 3 - Arrays and loops](img/arraysloops.png)



