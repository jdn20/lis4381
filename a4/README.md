> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Assignment 4 Requirements:

*2 Parts:*

1. Local LIS4381 Web App
2. Skillsets 10-12

#### README.md file should include the following items:

* Screenshot of Local Web App (Homepage and A4)
* Link to local lis4381: http://localhost:8080/repos/lis4381/

> 
> 
> 
>


#### Assignment Screenshots:

*Screenshot of Homepage:*

![Homepage of local app](img/homepage.png)

*Screenshot of Failed Validation*
![Failed validation](img/failedval.png)

*Screenshot of Passed Validation*
![Passed validation](img/passedval.png)

*Screenshot of Skillset 10 - Array List*
![Skillset 10](img/arraylist.png)

*Screenshot of Skillset 11 - Alpha Numeric Special*
![Skillset 11](img/asciivalue.png)

*Screenshot of Skillset 12 - Temperature Conversion*
![Skillset 12](img/tempconversion.png)


