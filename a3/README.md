> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Assignment 3 Requirements:

*3 Parts:*

1. Mobile App
2. Database
3. Skillsets 4-6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application’s opening user interface
* Screenshot of running application’s processing user input
* Screenshots of 10 records for each table—use select * from each table
* Links to database files 

> 
> 
> 
>

&nbsp;

#### Assignment Screenshots:

*Screenshots of "My Event" App:*

<table> 
    <tr>
        <th>Opening User Interface</th>
        <th>Processing User Input</th>
    </tr>
    <tr>
        <td><img src="img/myevent.png"></td>
        <td><img src="img/myevent2.png"></td>
    </tr>
</table> 

&nbsp;

&nbsp;

<table>
    <tr>
        <th>ERD Screenshot</th>
        <td><img src="img/erd.png"></td>
    </tr>
    <tr>
        <th>Petstore Records</th>
        <td><img src="img/pstrecords.png"></td>
    </tr>
    <tr>
       <th>Customer Records</th>
       <td><img src="img/cusrecords.png"></td>
    </tr>
    <tr>
        <th>Pet Records</th>
        <td><img src="img/petrecords.png"></td>
    </tr>
</table>

&nbsp;

&nbsp;


*Screenshot of Skillset 4 - Pseudorandom Number Generator:*
![Skillset 4](img/randomnumgenerator.png)

*Screenshot of Skillset 5 - Decision Structures:*
![Skillset 5](img/decisionstructures.png)

*Screenshot of Skillset 6 - Methods:*
![Skillset 6](img/methods.png)

*Link to a3.mwb file:*
[a3.mwb file](docs/a3.mwb "a3 mwb file")

*Link to a3.sql file:*
[a3.sql file](docs/a3.sql "a3 sql file")


