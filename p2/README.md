> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Project 2 Requirements:

*3 Parts:*

1. Local web app server-side validation
2. Edit and delete functionality for pet store table
3. RSS Feed

#### README.md file should include the following items:

* Screenshots of local web app using delete and update functions (with valid and invalid data)
* Link to local lis4381: http://localhost:8080/repos/lis4381/
  


> 
> 
> 
>


#### Assignment Screenshots:

*Screenshot of Index:*

![P2 Index Page](img/index.png)

&nbsp;

*Screenshot of Update Function:*

<table>
    <tr>
        <th>Failed Validation</th>
        <td><img src="img/edit_petstore.png"></td>
    </tr>
    <tr>
        <th>Error Message</th>
        <td><img src="img/error.png"></td>
    </tr>
    <tr>
        <th>Passed Validation</th>
        <td><img src="img/passed.png"></td>
    </tr>
</table>

&nbsp; 


*Screenshot of Delete Function:*

<table>
    <tr>
        <th>Delete Record Prompt</th>
        <td><img src="img/delete.png"></td>
    </tr>
    <tr>
        <th>Successful Deletion</th>
        <td><img src="img/success.png"></td>
    </tr>
</table>

*Screenshot of RSS Feed:*

![RSS Feed](img/rss.png)

