> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4381 - Mobile Web Application Development

## Janae Newton

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create a mobile recipe app using Android Studio
    - Create a Java program that evaluates integers as even or odd
    - Create a Java program that evaluates the larger of two integers
    - Create a Java program that loops through an array of strings

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create mobile app that calculates the cost of event tickets
    - Create a pet store database that stores data about stores, pets, and customers
    - Create a Java program that evaluates user-entered characters using if...else or switch statements
    - Create a Java program that displays pseudorandom-generated integers using various loop structures
    - Create a Java program that prompts user for information, then prints results using void method and value-returning method

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Set up a local web app demonstrating client-side validation
    - Create a Java program that prompts user for array values and displays array size & contents after every addition
    - Create a Java program that determines whether user-entered value is alpha, numeric, or special character
    - Create a Java program that converts user-entered temperatures into Fahrenheit or Celsius scales


5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Demonstrate server-side validation on local web app
    - Create a Java program that calculates sphere volume from user-entered diameter value
    - Create a PHP program that performs simple calculations
    - Create a PHP program that writes to and reads from the same file

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create a mobile app that displays a business card
    - Create a Java program that validates user input and displays pseudorandom-generated integers using various loop structures
    - Create a Java program that evaluates the largest of three integers
    - Create a Java program that creates array size at runtime, displays array size, and rounds sum and average of numbers to two decimal places

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Demonstrate server-side validation 
    - Add edit and delete functions to pet store table
    - Create RSS Feed