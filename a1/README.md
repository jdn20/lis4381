> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Assignment 1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation 
* Screenshot of running java Hello
* Screenshot of running Android Studio - My First App
* git commands w/short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - intializes a new git repository
2. git status - shows the current state of the repository
3. git add - adds files to the staging area 
4. git commit - records changes made to files in the local repo
5. git push - sends local commits to the remote repo
6. git pull - pulls changes from remote repo to the local computer
7. git clone - creates a copy of an existing remote repo

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost/phpinfo.php*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdkinstall.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jdn20/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jdn20/myteamquotes/ "My Team Quotes Tutorial")
