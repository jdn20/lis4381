> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Janae Newton

### Project 1 Requirements:

*2 Parts:*

1. Business Card Mobile App
2. Skillsets 7-9

#### README.md file should include the following items:

* Screenshot of running application's first user interface
* Screenshot of running application's second user interface



#### Project Screenshots:

*Screenshot of Business Card App:*

<table> 
    <tr>
        <th>First User Interface</th>
        <th>Second User Interface</th>
    </tr>
    <tr>
        <td><img src="img/mybusinesscard.png"></td>
        <td><img src="img/mybusinesscard2.png"></td>
    </tr>
</table> 

*Screenshot of Skillset 7 - Random Number Generator Data Validation:*
![Skillset 7](img/ss7.png)

*Screenshot of Skillset 8 - Larger Three Numbers:*
![Skillset 8](img/ss8.png)

*Screenshot of Skillset 9 - Array Runtime Data Validation:*
![Skillset 9](img/ss9.png)


